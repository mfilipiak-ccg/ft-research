![CCG Logo](./images/ccg_logo_small.png) 

Interested in working with us?\
Email us at: recruiting@chameleoncg.com

*Caveat: This document was created during initial investigation into the topic. Knowledge is assumed to be rudimentary*

# Goals

* [X] Deploy a simple kubernetes cluster locally
* [X] Explore k8s cluster networking

# Local Setup

Start the investigation locally. That way we can get familiar with kubernetes in general, without incurring any cost on AWS or other paid kubernetes service.

We will work with minikube locally. The setup I have is an Ubuntu VM (inside VMWare Workstation). I am choosing to use this VM and nesting virtualization inside of it, because I didn't want to install VirtualBox alongside Workstation on my Windows host.

```console
user@vm:~# apt-get install -y virtualbox

user@vm:~# curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
user@vm:~# chmod +x /usr/local/bin/kubectl

user@vm:~# curl -Lo /usr/local/bin/minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
user@vm:~# chmod +x /usr/local/bin/minikube

user@vm:~# minikube addons enable ingress
user@vm:~# minikube start
```

This will put us in a sufficient stage of inception (especially when running our cluster).

# Experiment

## Docker Baseline

We will have a simple setup. A client and server will be sufficient. In order to avoid writing code here, we will just use python's `SimpleHTTPServer` to demonstate exposing the server to the outside world. Assuming docker existing familiarity, we can start by a contrived implementation in docker to see the parallels.

```console
# run the server behind a dedicated network
user@vm:~# docker build -t alpine_python2:1.0 .
user@vm:~# docker network create local_http
user@vm:~# docker run -it --rm -p 8888:80 --network local_http --name http_srv alpine_python2:1.0 python -m SimpleHTTPServer 8888

# access within the network
user@vm:~# docker run -it --rm --network local_http alpine_python2:1.0 wget http_srv:8888

# access from host/dev VM (i.e public access, with port mapping)
user@vm:~# curl http://localhost
```

## Minikube

### Minikube - NGINX

Now let's get that running within minikube. Couple of concepts to translate from docker. All are fully described inside the http_server.yml file. 

* Pod will be our container running our `python` command
* Service allows us to expose the networking to the cluster
  * Since pods live & die and switch IP addresses as they do, services provide that stable point on which other resources can use as a dependency
  * https://kubernetes.io/docs/concepts/services-networking/service/
* Ingress provides external HTTP access (similar to a proxy)
  * https://kubernetes.io/docs/concepts/services-networking/ingress/

Refer to the [http_server_nginx.yml](http_server_nginx.yml) for the definition of the k8s cluster used below.

```console
# setup our docker environment to use the minikube repo so
# built containers are available to the cluster
user@vm:~# eval $(minikube docker-env)
user@vm:~# docker build -t alpine_python2:1.0 .

user@vm:~# minikube dashboard
user@vm:~# kubectl apply -f http_server_nginx.yml

# access within the cluster/network
user@vm:~# kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
http-649747976f-sbfkc   1/1     Running   0          14m
user@vm:~# kubectl exec -it http-649747976f-sbfkc -- /bin/bash
bash-5.0# wget localhost:8888

# access from host/dev VM (i.e public access, with port mapping)
user@vm:~# curl http://$(minikube ip)
```

This configuration was pieced together from articles found online. It just happens that we were using HTTP. The ingress portion of this configuration uses NGINX, which does not really match our docker setup we are trying to emulate. It is probably the appropriate way to handle this type of service, but what if we needed to expose the TCP port directly (i.e. expose a non-HTTP service).

### Minikube - Load Balancer

Looking at [http_server_load_balancer.yml](http_server_load_balancer.yml), we can see the new service definition. It is exactly the same as before, except we add the `type: LoadBalancer`. We also remove the ingress definition, as we will connect through the server 'directly'.

```console
user@vm:~# kubectl apply -f http_server_load_balancer.yml
user@vm:~# minikube service http-svc --url
http://192.168.99.103:32486
user@vm:~# curl http://192.168.99.103:32486
```

If we weren't using minikube, and need needed the ports that the service is bound to on the proxy, we can interrogate the service through kubectl. In the output below, you can see the port that will be mapped to the proxy. (Note: I believe the mapped port will always be in the range of 30000-32767).

```console
user@vm:~# kubectl describe services http-svc
Name:                     http-svc
Namespace:                default
<<redacted output>>
Type:                     LoadBalancer
IP:                       10.103.65.26
Port:                     http  80/TCP
TargetPort:               8888/TCP
NodePort:                 http  32486/TCP
Endpoints:                172.17.0.2:8888
```

# Tips

* It may be nice to have alpine with bash, since the minikube dashboard tries to console in using it. The console can be useful for debugging
* Starting with https://www.katacoda.com/courses/kubernetes might be a nice place to learn

# Next Steps

* [ ] Translate this to AWS (ECR)
* [ ] Combine this with an instance of EC2 (back-channel VPC connection?)
* [ ] Look at scaling within a cluster service
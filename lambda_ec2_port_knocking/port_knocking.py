import json
import boto3
import gzip
import base64
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

ENI_INDEX = 2
SRC_IP_INDEX = 3
DST_PORT_INDEX = 6

PORT_KNOCK_SEQUENCE = ['2525']

def add_ssh_for_ip(ip, eni):
    client = boto3.client('ec2')
    eni = client.describe_network_interfaces(NetworkInterfaceIds=[eni])
    # XXX assuming a single security group for simplicity
    sec_group_id = eni['NetworkInterfaces'][0]['Groups'][0]['GroupId']
    response = client.authorize_security_group_ingress(
        GroupId=sec_group_id,
        IpPermissions=[{
            'IpProtocol': 'tcp',
            'FromPort': 22,
            'ToPort': 22,
            'IpRanges': [{
                'CidrIp': ip + '/32', 
                'Description': "Answered the port knock"
            }],
        }]
    )

def lambda_handler(event, context):
    # logger.info(event) # useful to copy for the lambda test reply harness
    data = base64.b64decode(event.get('awslogs').get('data'))
    decompressed_data = gzip.decompress(data)
    log_events = json.loads(decompressed_data).get('logEvents', [])
    
    for event in log_events:
        # https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html#flow-log-records
        event_fields = event.get('message', '').split(' ')
        
        # not doing sequences, because that would require state, so just knock 
        # on the single port for now
        if event_fields[DST_PORT_INDEX] == PORT_KNOCK_SEQUENCE[0]:
            logger.info("UNLOCKING SSH for IP: " + event_fields[SRC_IP_INDEX])
            eni = event_fields[ENI_INDEX]
            add_ssh_for_ip(event_fields[SRC_IP_INDEX], eni)
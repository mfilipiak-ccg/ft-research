![CCG Logo](./images/ccg_logo_small.png) 

Interested in working with us?\
Email us at: recruiting@chameleoncg.com

# AWS Port Knocking using Lambda

This is not a recommended 'security' strategy, although it may have a place. This is merely a case study in order to get familiar with some AWS services. After writing this, a google search shows people are indeed attempting to use similiar technique for real.

The basic idea is that the security groups attached to the instance is by default blocking ALL ingress traffic (including SSH). Scanners will see no service respond on port 22, unless the appropriate port is 'knocked' first. The simple case will use a single port knock, but sequences are preferred.

One goal of this implementation is to keep the port knocking separate from the instance itself (i.e. nothing special loaded by the instance). To do this, we can use flow logs, and lambdas to check the ports being knocked. If it matches, the lambda function will punch a hole in the security group for the IP. You could also have a sequence to revoke the rule if you wanted.
  
## Technologies

The motiviation is to get some actual experience using the following AWS services.

* EC2 
* Lambda
* Flow logs for knocking monitoring (not ideal)
* CloudWatch for forwarding logs to lambda
* IAM

### Listening for knocks on the VPC

The goal is to find a solution that is mostly AWS based, without deploying a lot of custom sensors. This is not an elegant approach, since the logs may not emit for 10-15 minutes. In many cases, mirroring the VPC traffic to a service on EC2 to broadcast to a lambda consumable channel is going to be more real time.

https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html

IAM settings:
https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs-cwl.html

### Lambda

Using python 3.x. Need to setup the IAM rules so that when we detect a successful knock, we can modify the security group. Modifying the security group involves the following.

* Use the ENI in the flow log to determine the active security groups (query)
* Modify the security group to allow the src IP to access port 22

Here's the IAM rule.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:DescribeNetworkInterfaces"
            ],
            "Resource": "*"
        }
    ]
}
```

The lambda function is fairly simple. Open the [python source file](port_knocking.py) to see the implementation. Note, most of the time spent in the lambda function will be making the AWS calls.

## Putting everything together

* Assuming we have a deployment described by the [terraform file](east_region_vpc.tf)
  * Terraform/CloudFormation for the flow-logs and lambda to come later
* Create an IAM rule for flow logs and CloudWatch to talk to each other (see above link)
* Create a log group within CloudWatch that will forward to the lambda function
* Create the lambda function and setup the IAM rule in the snippet above
* Add a trigger for the lambda function, which will be the CloudWatch log group
    * Note the outputs to CloudWatch will be lambda log messages
    * Note the outputs to EC2 are policy based on modifying the security groups

![lambda](images/lambda_flow.png)

* Go to the VPC or ENI to monitor and add a new flow log
    * Select the CloudWatch log group and IAM role
    * Select Reject Traffic

![vpcflow](images/VPC_flow.png)

# Testing

Check the instance's security group's ingress rules, and ensure there are not ALLOW rules. Grab the instance's public IP (Using 5.5.5.5 for this example).

A user wishing to punch a hole, without the ability to use an AWS tool, could perform something like the following.

```console
# Will fail, because the security group is blocking the port
user@vm:~# ssh -i ~/.ssh/aws.pem ec2-user@5.5.5.5
user@vm:~# ssh -i ~/.ssh/aws.pem ec2-user@5.5.5.5 -p 2525

# watch a period of time (because of the flow-log lag)
user@vm:~# ssh -i ~/.ssh/aws.pem ec2-user@5.5.5.5
ec2> 
```

More importantly, random SSH brute force attacks will be denied. Scanners are somewhat protected.

```console
user@vm:~# nmap -sV -p 22,2525 3.18.104.52
Host is up (0.0046s latency).

PORT     STATE    SERVICE     VERSION
22/tcp   filtered ssh
2525/tcp filtered ms-v-worlds
```

Since we did not do a sequence for the port knocking, any sweeping scan can be treated as a successful 'knock'.  This means rescanning from the same IP will likely be successful. We can look into solving this by learning about step functions a follow-on.

```console
user@vm:~# nmap -sV -p 22 3.18.104.52
Host is up (0.0046s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)
```

## Notes

### Testing

The most efficient way to test is to capture one test event and use the test feature in the lambda editor. Especially for events that may not be coming in frequently. Even better would be to capture the event and mock the context to unit test within the real source.

### Debugging

As you can see in the Lambda 'Designer', logs from the lambda function can be viewed in the CloudWatch Log group for the Lambda.
# Goals

Testing out EC2 VPCs to understand their options better. 

* [x] Investigating the Layer 2 behavior
    * [x] Inter-region Layer 2 behavior of VPC
    * [x] Investigate options connecting VPCs and instances between regions
* [x] What control we have with ACLs to emulate things like PVLAN's community/isolated structure
* [x] Using terraform to have a repeatable setup

## Stretch Goals

* [ ] Understand Load Balancing
* [ ] Understand different options for persistence
* [ ] Understand auto scaling options
* [ ] Placement groups

# Sandbox Setup

Use terraform to setup the instance. You need your own AWS key. The terraform
wrapper script will use this automatically.

```console
user@vm:~# cat ~/.aws/credentials
[default]                                                     
aws_access_key_id=YOUR_KEY
aws_secret_access_key=YOUR_KEY
```

Now you can just run the terraform apply script to setup the environment for
personal exploration.

```console
user@vm:~/src# ./terraform init
user@vm:~/src# ./terraform apply -state=east east_region_vpc.tf

# can view IPs created, etc (can just look at the state file as well)
user@vm:~/src# ./terraform show -state=east
```

# Findings

In general a lot of time was spent getting comfortable with terraform (or EC2 where non-default configurations were requested). The terraform (.tf) files in this repo may be used as a starting point for instances that are configured with a public IP with SSH enabled. 

## VPCs support for Layer 2

https://aws.amazon.com/answers/networking/aws-multiple-region-multi-vpc-connectivity/

### Same Regions

Short answer, yes VPCs operate at Layer 2 (not overly surprising). Setting up the environment in the terraform file will create a 2 node network behind the VPC. Each ARP table shows the IPs specified on the other node.

### Across Regions

There are different connection strategies for VPCs. Peering being the most interesting, as it is supported out of the box, and potentially off internet. Of course, you can handle whatever you want from implementing your own VXLAN or VPN structure.

## Peering

Two VPCs can be peered together, and you are then able to reach the other machine via the private IP. The documentation makes a distinction between a gateway and peering. In configuration, it certainly feels as though you are setting up a gateway. Peering will operate at Layer 3.

https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.html

* Cannot have overlapping subnets between peered VPCs
* Encrypted during transport
* Can peer between VPCs in same region, different regions, and VPCs from _different accounts_

### Peering Setup

These terraform files likely need some refactoring. Running this setup in 2 stages gave me the appropriate setup to play with peering manually through the console. 

Peering is possible directly through terraform as well:
https://www.terraform.io/docs/providers/aws/r/vpc_peering.html

```console
user@vm:~# ./terraform apply -state=east.tfstate
user@vm:~# ./terraform apply -state=west.tfstate west-region
```

Now you can go into the console for each region. You need to setup this configuration from both VPC and/or regions.

* Navigate to the VPC section
* Choose one region to initiate the peering
  * Create the peering connection and enter the relevant information
  * `./terraform show west.tfstate | grep "vpc_id"` can be useful
* Accept the peering connection on the other region
* Go back to your VPC and ensure the associated subnet has a route to the CIDR of the other VPC is routed to the peer connection
* Ensure the VPC's ACLs allow the connection
* Go to the security group of the target instance(s) within the VPC
  * Ensure they allow the desired traffic (e.g. ICMP or SSH)
* Check the VPC and security group settings on the opposing VPC
* Test private IPs are reachable from the instances

## ACLs for PVLAN style isolation

Fairly straight forward for Layer 3 control. The Security Groups support most of the use cases I can imagine. One difference to the PVLAN structure is the Layer 2 isolation. Probably worth putting this investigation on the backburner, and accepting Security Groups and VPC level ACLs are sufficient for 99% of the use cases.

## Troubleshooting

Useful resources:

* https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html
* https://us-east-2.console.aws.amazon.com/vpc

### Cannot reach EC2 node (ICMP or SSH punched in security groups)

Go the the vpc console (linked above). Then navigate to Internet Gateways.  There you can make sure there is a gateway to the public internet. Initial problem was there was an assigned public IP via `map_public_ip_on_launch` option, but there was no internet gateway attached to the VPC. Basically needed
a route on the VPC to `0.0.0.0/0` and assigned to the internet gateway.
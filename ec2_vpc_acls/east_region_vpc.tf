# Configure the AWS Provider
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-2"
  shared_credentials_file = "/src/.aws/credentials"
}

# Create a VPC to connect our instances to
resource "aws_vpc" "tf_vpc" {
  cidr_block = "172.16.0.0/24"
}

# This is needed to provide internet access (e.g SSH)
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.tf_vpc.id}"
}

# Security group with inbound=SSH, outbound=*
resource "aws_security_group" "allow_all_tf" {
  name        = "allow_all_tf"
  description = "Allow all inbound traffic (Terraform)"
  vpc_id      = "${aws_vpc.tf_vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

# Subnet for the VPC, also maps each IP to a public IP
# which will give us SSH
resource "aws_subnet" "tf_subnet" {
  vpc_id            = "${aws_vpc.tf_vpc.id}"
  cidr_block        = "172.16.0.0/24"
  map_public_ip_on_launch = true
}

# Ensure we have a default route out of the VPC to
# public internet gateway
resource "aws_route_table" "route-table-tf" {
  vpc_id = "${aws_vpc.tf_vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

# Add the internet gateway to the subnet/VPC
resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.tf_subnet.id}"
  route_table_id = "${aws_route_table.route-table-tf.id}"
}

resource "aws_instance" "srv-a" {
  ami             = "ami-0ebbf2179e615c338"
  instance_type   = "t2.micro"
  key_name = "ccg"
  subnet_id = "${aws_subnet.tf_subnet.id}"
  security_groups = ["${aws_security_group.allow_all_tf.id}"]
}

resource "aws_instance" "srv-b" {
  ami             = "ami-0ebbf2179e615c338"
  instance_type   = "t2.micro"
  key_name = "ccg"
  subnet_id = "${aws_subnet.tf_subnet.id}"
  security_groups = ["${aws_security_group.allow_all_tf.id}"]
}